# Operating System Builder

## Usage

Enter
``` sh
osbuilder help
```
for help.

## Building

``` sh
./gradlew installDist
```
