package org.fnet.osbuilder.arguments;

import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ArgumentParserTest {

	@Test
	public void parse() {
		ArgumentParser parser = new ArgumentParser();
		parser.addFlag("s", "save");
		parser.addFlag("other");
		parser.addParameter("param");
		parser.addParameter("l", "test");

		Arguments args = parser.parse(new String[]{
				"-s", "--other", "test", "--param", "testing", "-l", "othertest", "remain"
		});
		assertEquals(List.of("s", "other"), args.getFlags());
		assertEquals(Map.of("param", "testing", "l", "othertest"), args.getParameters());
		assertEquals(List.of("test", "remain"), args.getRemaining());
	}

	@Test(expected = IllegalArgumentException.class)
	public void parse_fail_noValue() {
		ArgumentParser parser = new ArgumentParser();
		parser.addParameter("test");
		parser.addFlag("second");
		parser.parse(new String[]{"--test", "--second"});
	}

	@Test(expected = IllegalArgumentException.class)
	public void parse_fail_noValueAtEnd() {
		ArgumentParser parser = new ArgumentParser();
		parser.addParameter("test");
		parser.parse(new String[]{"--test"});
	}

	@Test(expected = IllegalArgumentException.class)
	public void parse_fail_argumentNotFound() {
		ArgumentParser parser = new ArgumentParser();
		parser.parse(new String[]{"--test"});
	}

	@Test(expected = RuntimeException.class)
	public void parse_fail_alreadyContained() {
		ArgumentParser parser = new ArgumentParser();
		parser.addParameter("test");
		parser.addParameter("test");
	}

}