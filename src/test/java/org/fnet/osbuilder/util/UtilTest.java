package org.fnet.osbuilder.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilTest {

	@Test
	public void joinArrays() {
		Object[] a = new Object[] { "test1", "test2" };
		Object[] b = new Object[] { "test3", "test4" };
		Object[] result = Util.joinArrays(a, b);
		assertArrayEquals(new Object[] { "test1", "test2", "test3", "test4" }, result);
	}

	@Test
	public void joinArrays_multi() {
		Object[] a = new Object[] { "test1", "test2" };
		Object[] b = new Object[] { "test3", "test4", "testX" };
		Object[] c = new Object[] { "test5", "test6" };
		Object[] result = Util.joinArrays(a, b, c);
		assertArrayEquals(new Object[] { "test1", "test2", "test3", "test4", "testX", "test5", "test6" }, result);
	}

	@Test
	public void joinArrays_empty() {
		Object[] result = Util.joinArrays();
		assertEquals(0, result.length);
	}

	@Test
	public void joinArrays_single() {
		Object[] a = new Object[] { "test1", "test2" };
		Object[] result = Util.joinArrays(a);
		assertArrayEquals(a, result);

	}
}