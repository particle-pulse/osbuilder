package org.fnet.osbuilder.util;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class StringFormatterTest {

	@Test
	public void format() {
		StringFormatter formatter = new StringFormatter();
		Map<String, String> parameters = new HashMap<>();
		parameters.put("test1", "Result 1");
		parameters.put("test2", "18");
		String result = formatter.format("${test1} is ${test2}", parameters);
		assertEquals("Result 1 is 18", result);
	}

	@Test(expected = NullPointerException.class)
	public void format_notContainedInParameters() {
		StringFormatter formatter = new StringFormatter();
		formatter.format("${doesnt_exist}", Map.of());
	}
}