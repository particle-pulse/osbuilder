package org.fnet.osbuilder.toolchain.dependencies;

import org.fnet.osbuilder.toolchain.*;
import org.fnet.osbuilder.toolchain.dependencies.DependencyGraph.Node;
import org.fnet.osbuilder.toolchain.repositories.ArtifactNotFoundException;
import org.fnet.osbuilder.toolchain.repositories.Repositories;

public class ToolchainDependencyGraph {

	private DependencyGraph<InstalledToolchainComponent> graph = new DependencyGraph<>();
	private Toolchain toolchain;

	public ToolchainDependencyGraph(Toolchain toolchain) {
		this.toolchain = toolchain;
	}

	public Node<InstalledToolchainComponent> add(InstalledToolchainComponent component) throws ArtifactNotFoundException {
		Node<InstalledToolchainComponent> node = new Node<>(component);
		for (String dependency : component.getComponent().getDependencies()) {
			ToolchainComponent dependencyComponent = IComponentFactory.getFactory(dependency).get(toolchain);
			Node<InstalledToolchainComponent> installedDependency = null;
			for (Node<InstalledToolchainComponent> gn : graph.getNodes()) {
				if (gn.value.getComponent().equals(dependencyComponent)) {
					installedDependency = gn;
					break;
				}
			}
			if (installedDependency == null) {
				InstalledToolchainComponent installedToolchainComponent = new InstalledToolchainComponent(
						dependencyComponent, Repositories.getRepositories().getLatest(dependency));
				installedDependency = add(installedToolchainComponent);
			}
			node.addEdge(installedDependency);
		}
		graph.addNode(node);
		return node;
	}

	public DependencyGraph<InstalledToolchainComponent> getGraph() {
		return graph;
	}
}
