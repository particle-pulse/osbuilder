package org.fnet.osbuilder.toolchain.tasks;

import org.fnet.osbuilder.Main;
import org.fnet.osbuilder.systemtools.Extractor;
import org.fnet.osbuilder.systemtools.Tool;
import org.fnet.osbuilder.util.Util;

import java.io.File;

public class ExtractTask implements ITask<File, File> {

	private File targetDirectory;
	private int stripLevel = 0;

	public ExtractTask(File targetDirectory, int stripLevel) {
		this.targetDirectory = targetDirectory;
		this.stripLevel = stripLevel;
	}

	public ExtractTask(int stripLevel) {
		this.targetDirectory = null;
		this.stripLevel = stripLevel;
	}

	public ExtractTask(File targetDirectory) {
		this.targetDirectory = targetDirectory;
	}

	public ExtractTask() {
		this.targetDirectory = null;
	}

	private String getCacheFileName(File file) {
		return String.format("%d_%s", file.lastModified(), file.getName());
	}

	@Override
	public File execute(File input) throws Exception {
		if (targetDirectory == null)
			targetDirectory = new File(Main.getApplication().getTempDirectory(), getCacheFileName(input));
		Util.createDirectory(targetDirectory);

		Tool.getTool(Extractor.class, e -> e.supports(input))
				.extract(input, targetDirectory, true, stripLevel);
		return targetDirectory;
	}
}
