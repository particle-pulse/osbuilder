package org.fnet.osbuilder.toolchain.tasks;

import java.io.File;

public interface ITask<I, O> {

	O execute(I input) throws Exception;

}
