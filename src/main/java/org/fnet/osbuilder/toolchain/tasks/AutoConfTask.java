package org.fnet.osbuilder.toolchain.tasks;

import org.fnet.osbuilder.ProcessRunner;
import org.fnet.osbuilder.util.Util;

import java.io.File;
import java.util.function.Consumer;

public class AutoConfTask implements ITask<File, File> {

	private ProcessRunner runner;
	private Object[] arguments;
	private File targetDirectory;
	private boolean createNewDirectory;
	private File newDirectory;
	private boolean forceRebuild;

	public AutoConfTask(File targetDirectory, boolean createNewDirectory, Object... arguments) {
		this.runner = new ProcessRunner();
		this.arguments = arguments;
		this.targetDirectory = targetDirectory;
		this.createNewDirectory = createNewDirectory;
	}

	public AutoConfTask(File targetDirectory, Object... arguments) {
		this.runner = new ProcessRunner();
		this.arguments = arguments;
		this.targetDirectory = targetDirectory;
		this.createNewDirectory = false;
	}

	public AutoConfTask(File targetDirectory, File buildDirectory, Object... arguments) {
		this.runner = new ProcessRunner();
		this.arguments = arguments;
		this.targetDirectory = targetDirectory;
		this.createNewDirectory = true;
		this.newDirectory = buildDirectory;
	}

	@Override
	public File execute(File input) throws Exception {
		if (createNewDirectory) {
			if (newDirectory == null)
				newDirectory = new File(input, "build_dir");
			Util.createDirectory(newDirectory);
			runner.setDirectory(newDirectory);
		} else {
			runner.setDirectory(newDirectory = input);
		}
		Object[] defaultArguments = new Object[]{
				"--prefix", targetDirectory
		};
		if (forceRebuild || !new File(newDirectory, "Makefile").exists())
			runner.run(new File(input, "configure"), Util.joinArrays(defaultArguments, arguments));
		return createNewDirectory ? newDirectory : input;
	}

	public ProcessRunner getRunner() {
		return runner;
	}

	public AutoConfTask withRunner(Consumer<ProcessRunner> runner) {
		runner.accept(this.runner);
		return this;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public File getTargetDirectory() {
		return targetDirectory;
	}

	public boolean isCreateNewDirectory() {
		return createNewDirectory;
	}

	public File getNewDirectory() {
		return newDirectory;
	}

	public boolean isForceRebuild() {
		return forceRebuild;
	}

	public AutoConfTask setForceRebuild(boolean forceRebuild) {
		this.forceRebuild = forceRebuild;
		return this;
	}
}
