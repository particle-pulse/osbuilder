package org.fnet.osbuilder.toolchain.tasks;

import org.fnet.osbuilder.ProcessRunner;

import java.io.File;

public class RunTask implements ITask<File, File> {

	private String executable;
	private Object[] arguments;
	ProcessRunner runner = new ProcessRunner();

	public RunTask(String executable, Object... arguments) {
		this.executable = executable;
		this.arguments = arguments;
	}

	public RunTask(File executable, Object... arguments) {
		this.executable = executable.getAbsolutePath();
		this.arguments = arguments;
	}

	public String getExecutable() {
		return executable;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public ProcessRunner getRunner() {
		return runner;
	}

	public RunTask setRunner(ProcessRunner runner) {
		this.runner = runner;
		return this;
	}

	@Override
	public File execute(File input) throws Exception {
		runner.setDirectory(input.isDirectory() ? input : input.getParentFile());
		runner.run(executable, arguments);
		return input;
	}
}
