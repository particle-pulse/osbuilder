package org.fnet.osbuilder.toolchain.tasks;

public class TaskChain<I> {

	private I value;

	public TaskChain(I value) {
		this.value = value;
	}

	public static <I> TaskChain<I> chain(I initialValue) {
		return new TaskChain<>(initialValue);
	}

	public <O> TaskChain<O> then(ITask<I, O> task) throws Exception {
		return new TaskChain<>(task.execute(value));
	}

	public I get() {
		return value;
	}

}
