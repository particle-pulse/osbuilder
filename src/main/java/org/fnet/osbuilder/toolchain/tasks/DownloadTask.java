package org.fnet.osbuilder.toolchain.tasks;

import org.fnet.osbuilder.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class DownloadTask implements ITask<URL, File> {

	@Override
	public File execute(URL input) throws IOException {
		return Main.getApplication().getCachedOrDownload(input);
	}
}
