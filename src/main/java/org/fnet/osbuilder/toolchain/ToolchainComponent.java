package org.fnet.osbuilder.toolchain;

import com.vdurmont.semver4j.Semver;
import org.fnet.osbuilder.toolchain.repositories.ArtifactNotFoundException;
import org.fnet.osbuilder.toolchain.repositories.Repositories;
import org.fnet.osbuilder.util.Util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class ToolchainComponent {

	@FunctionalInterface
	public interface BuildProcedure {
		void build(URL archiveURL, File srcDir, File buildDir, Toolchain toolchain) throws Exception;
	}

	private String artifactID, displayName;
	private Set<String> dependencies = new HashSet<>();
	private BuildProcedure procedure;

	private Toolchain toolchain;
	private File rootDirectory, sourceDirectory, buildDirectory;

	public ToolchainComponent(String artifactID, String displayName, BuildProcedure procedure, Toolchain toolchain) {
		this.artifactID = artifactID;
		this.displayName = displayName;
		this.procedure = procedure;
		this.toolchain = toolchain;
		this.rootDirectory = new File(toolchain.getDirectory(), getArtifactID());
		this.sourceDirectory = new File(rootDirectory, "src");
		this.buildDirectory = new File(rootDirectory, "build");
	}

	public void build(Semver version) throws Exception {
		procedure.build(Repositories.getRepositories().getArchiveURL(getArtifactID(), version), sourceDirectory, buildDirectory, toolchain);
	}

	public void remove() throws IOException {
		Util.deleteRecursive(rootDirectory);
	}

	public String getArtifactID() {
		return artifactID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Set<String> getDependencies() {
		return dependencies;
	}

	public BuildProcedure getProcedure() {
		return procedure;
	}

	public Toolchain getToolchain() {
		return toolchain;
	}

	public File getSourceDirectory() {
		return sourceDirectory;
	}

	public File getRootDirectory() {
		return rootDirectory;
	}

	public File getBuildDirectory() {
		return buildDirectory;
	}
}
