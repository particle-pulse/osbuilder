package org.fnet.osbuilder.toolchain.impl;

import com.google.auto.service.AutoService;
import org.fnet.osbuilder.toolchain.IComponentFactory;
import org.fnet.osbuilder.toolchain.Toolchain;
import org.fnet.osbuilder.toolchain.ToolchainComponent;
import org.fnet.osbuilder.toolchain.tasks.*;

@AutoService(IComponentFactory.class)
public class BinutilsComponentFactory implements IComponentFactory {

	@Override
	public String getID() {
		return "binutils";
	}

	@Override
	public ToolchainComponent get(Toolchain t) {
		return new ToolchainComponent(
				getID(),
				"Binutils",
				(archiveUrl, srcDir, buildDir, toolchain) ->
						TaskChain.chain(archiveUrl)
						.then(new DownloadTask())
						.then(new ExtractTask(srcDir, 1))
						.then(new AutoConfTask(toolchain.getTargetDirectory(), buildDir,
								"--target", toolchain.getTarget(), "--with-sysroot", "--disable-nls",
								"--disable-werror"))
						.then(new RunTask("make", "-j4"))
						.then(new RunTask("make", "install", "-j2")),
				t);
	}

}
