package org.fnet.osbuilder.toolchain.impl;

import com.google.auto.service.AutoService;
import org.fnet.osbuilder.toolchain.IComponentFactory;
import org.fnet.osbuilder.toolchain.Toolchain;
import org.fnet.osbuilder.toolchain.ToolchainComponent;
import org.fnet.osbuilder.toolchain.tasks.*;

import java.io.File;

@AutoService(IComponentFactory.class)
public class GCCComponentFactory implements IComponentFactory {
	@Override
	public String getID() {
		return "gcc";
	}

	@Override
	public ToolchainComponent get(Toolchain t) {
		ToolchainComponent component = new ToolchainComponent(
				getID(),
				"GNU Compiler Collection",
				(archiveUrl, srcDir, buildDir, toolchain) -> {
					TaskChain.chain(archiveUrl)
							.then(new DownloadTask())
							.then(new ExtractTask(srcDir, 1))
							.then(new RunTask("contrib/download_prerequisites"))
							.then(new AutoConfTask(toolchain.getTargetDirectory(), buildDir,
									"--target", toolchain.getTarget(), "--disable-nls",
									"--enable-languages=c,c++", "--without-headers")
									.withRunner(r -> r.exportPath(new File(toolchain.getTargetDirectory(), "bin"))))
							.then(new RunTask("make", "all-gcc", "-j4"))
							.then(new RunTask("make", "all-target-libgcc", "-j2"))
							.then(new RunTask("make", "install-gcc", "-j2"))
							.then(new RunTask("make", "install-target-libgcc", "-j2"));
				},
				t
		);
		component.getDependencies().add("binutils");
		return component;
	}
}
