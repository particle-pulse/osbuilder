package org.fnet.osbuilder.toolchain;

import java.util.ServiceLoader;

public interface IComponentFactory {

	static IComponentFactory getFactory(String id) {
		for (IComponentFactory factory : ServiceLoader.load(IComponentFactory.class))
			if (factory.getID().equals(id))
				return factory;
		return null;
	}

	String getID();

	ToolchainComponent get(Toolchain t);

}
