package org.fnet.osbuilder.toolchain.repositories;

import com.google.auto.service.AutoService;
import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.SemverException;
import org.fnet.osbuilder.toolchain.repositories.utilities.DirectoryListing;
import org.fnet.osbuilder.util.Uncheck;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@AutoService(Repository.class)
public class NASMRepository implements Repository {

	private static final URL NASM_REPO_URL = Uncheck.uncheck(() -> new URL("https://www.nasm.us/pub/nasm/releasebuilds/"));

	@Override
	public Semver[] listVersions(String id) throws ArtifactNotFoundException {
		if (!id.equals("nasm"))
			throw new ArtifactNotFoundException(id + " is not in NASM repository");
		DirectoryListing listing = Uncheck.uncheck(() -> new DirectoryListing(NASM_REPO_URL));
		List<Semver> versions = new ArrayList<>();
		for (DirectoryListing.RemoteFile remoteFile : listing.getRemoteFiles()) {
			try {
				Semver semver = new Semver(remoteFile.getName(), Semver.SemverType.LOOSE);
				versions.add(semver);
			} catch (SemverException ignored) {
				// Ignoring invalid (rc) versions for the time
			}
		}
		return versions.toArray(new Semver[0]);
	}

	@Override
	public URL getArchiveURL(String id, Semver version) throws ArtifactNotFoundException {
		if (!id.equals("nasm"))
			throw new ArtifactNotFoundException(id + " is not in NASM repository");
		return Uncheck.uncheck(() -> new URL(NASM_REPO_URL, MessageFormat.format("{0}/{1}-{0}.tar.xz",
				version.getOriginalValue(), id)));
	}
}
