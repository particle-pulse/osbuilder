package org.fnet.osbuilder.toolchain.repositories;

import com.vdurmont.semver4j.Semver;

import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

public interface Repository {

	Semver[] listVersions(String id) throws ArtifactNotFoundException;

	default Semver getLatest(String id) throws ArtifactNotFoundException {
		return Arrays.stream(listVersions(id)).max(Comparator.comparing(e -> e)).orElse(null);
	}

	URL getArchiveURL(String id, Semver version) throws ArtifactNotFoundException;

}
