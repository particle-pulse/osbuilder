package org.fnet.osbuilder.util;

import org.fnet.osbuilder.Main;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Util {

	public static void createDirectory(File directory) throws IOException {
		if (!directory.exists() && !directory.mkdirs())
			throw new IOException("Could not create directory " + directory);
	}

	public static ProcessBuilder defaultProcessBuilder(List<String> args) {
		return new ProcessBuilder(args).redirectOutput(Redirect.INHERIT).redirectError(Redirect.INHERIT);
	}

	public static List<File> listRecursive(File file) {
		List<File> fileList = new ArrayList<>();
		for (File f : Objects.requireNonNull(file.listFiles(e -> !e.equals(file)))) {
			if (f.isDirectory()) {
				if (!Files.isSymbolicLink(f.toPath()))
					fileList.addAll(listRecursive(f));
			} else {
				fileList.add(f);
			}
		}
		return fileList;
	}

	public static List<File> listRecursive(File file, FileFilter filter) {
		List<File> fileList = new ArrayList<>();
		for (File f : Objects.requireNonNull(file.listFiles(f -> f.isDirectory() || filter.accept(f)))) {
			if (f.isDirectory())
				fileList.addAll(listRecursive(f, filter));
			else
				fileList.add(f);
		}
		return fileList;
	}

	public static void deleteRecursive(File file) throws IOException {
		for (File f : Objects.requireNonNull(file.listFiles())) {
			if (f.isDirectory()) {
				deleteRecursive(f);
			} else {
				if (!f.delete())
					throw new IOException("Could not delete " + file.toString());
			}
		}
		if (!file.delete())
			throw new IOException("Could not delete " + file.toString());
	}

	public static boolean isRemoteNewer(File file, URL url) throws IOException {
		return isRemoteNewer(file, url, false);
	}

	public static boolean isRemoteNewer(File file, URL url, boolean failOnError) throws IOException {
		if (Main.getApplication().getArguments().getFlags().contains("offline"))
			return false;
		ZonedDateTime remoteModified;
		try {
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("HEAD");

			if (con.getResponseCode() != 200)
				throw new IOException("Expected response code 200, got " + con.getResponseCode());

			remoteModified = LocalDateTime
					.parse(con.getHeaderField("Last-Modified"), DateTimeFormatter.RFC_1123_DATE_TIME)
					.atZone(ZoneId.systemDefault());

			con.disconnect();
		} catch (IOException e) {
			if (failOnError) {
				throw e;
			} else {
				Logger.warn("Could not check if remote is newer: " + e.getMessage());
				return false;
			}
		}
		FileTime localModified = Files.getLastModifiedTime(file.toPath());
		return localModified.toInstant().isAfter(remoteModified.toInstant());
	}

	public static final File PROGRAM_DIRECTORY;

	public static final File TEMP_DIRECTORY;

	static {
		try {
			File progDir = new File(Util.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
			if (progDir.getName().endsWith("lib"))
				progDir = progDir.getParentFile();
			PROGRAM_DIRECTORY = progDir;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}

		TEMP_DIRECTORY = new File(PROGRAM_DIRECTORY, "temp");
	}

	public static Object[] joinArrays(Object[] a, Object[] b) {
		Object[] newArray = new Object[a.length + b.length];
		System.arraycopy(a, 0, newArray, 0, a.length);
		System.arraycopy(b, 0, newArray, a.length, b.length);
		return newArray;
	}

	public static Object[] joinArrays(Object[]... arrays) {
		int length = 0;
		for (Object[] array : arrays)
			length += array.length;
		Object[] newArray = new Object[length];
		int prevLength = 0;
		for (Object[] array : arrays) {
			System.arraycopy(array, 0, newArray, prevLength, array.length);
			prevLength += array.length;
		}
		return newArray;
	}
}
