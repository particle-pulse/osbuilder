package org.fnet.osbuilder.arguments;

public class ArgumentDefinition {
	public enum ArgumentType {
		PARAMETER, FLAG
	}

	private String shortForm, longForm;
	private ArgumentType type;

	public ArgumentDefinition(String shortForm, String longForm, ArgumentType type) {
		this.shortForm = shortForm;
		this.longForm = longForm;
		this.type = type;
	}

	public String getShortForm() {
		return shortForm;
	}

	public String getLongForm() {
		return longForm;
	}

	public ArgumentType getType() {
		return type;
	}
}
