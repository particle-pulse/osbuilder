package org.fnet.osbuilder.arguments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgumentParser {

	private static final String LONG_PREFIX = "--";
	private static final String SHORT_PREFIX = "-";

	private List<ArgumentDefinition> parameters = new ArrayList<>();

	public void addParameter(String shortForm, String longForm) {
		add(new ArgumentDefinition(shortForm, longForm, ArgumentDefinition.ArgumentType.PARAMETER));
	}

	public void addParameter(String longForm) {
		addParameter(null, longForm);
	}

	public void addFlag(String shortForm, String longForm) {
		add(new ArgumentDefinition(shortForm, longForm, ArgumentDefinition.ArgumentType.FLAG));
	}

	public void addFlag(String longForm) {
		addFlag(null, longForm);
	}

	public void add(ArgumentDefinition def) {
		for (ArgumentDefinition definition : parameters) {
			if (definition.getLongForm() != null && definition.getLongForm().equals(def.getLongForm()))
				throw new RuntimeException("Argument with same long form already registered");
			if (definition.getShortForm() != null && definition.getShortForm().equals(def.getShortForm()))
				throw new RuntimeException("Argument with same short form already registered");
		}
		parameters.add(def);
	}

	private ArgumentDefinition getDefinition(String name, boolean shortForm) {
		if (shortForm) {
			for (ArgumentDefinition def : parameters)
				if (def.getShortForm() != null && def.getShortForm().equals(name))
					return def;
		} else {
			for (ArgumentDefinition def : parameters)
				if (def.getLongForm() != null && def.getLongForm().equals(name))
					return def;
		}
		return null;
	}

	public Arguments parse(String[] args) {
		List<String> flags = new ArrayList<>();
		Map<String, String> parameters = new HashMap<>();
		List<String> remaining = new ArrayList<>();
		for (int i = 0; i < args.length; i++) {
			String argument = args[i];
			if (argument.startsWith(LONG_PREFIX)) {
				String name = argument.substring(LONG_PREFIX.length());
				ArgumentDefinition definition = getDefinition(name, false);
				if (definition == null)
					throw new IllegalArgumentException(name);
				if (definition.getType() == ArgumentDefinition.ArgumentType.FLAG) {
					flags.add(name);
				} else {
					if (i == args.length - 1)
						throw new IllegalArgumentException(name + " expects a value");
					String next = args[i + 1];
					if (next.startsWith(LONG_PREFIX) || next.startsWith(SHORT_PREFIX))
						throw new IllegalArgumentException(name + " expects a value");
					parameters.put(name, next);
					i++;
				}
			} else if (argument.startsWith(SHORT_PREFIX)) {
				String name = argument.substring(SHORT_PREFIX.length());
				ArgumentDefinition definition = getDefinition(name, true);
				if (definition == null)
					throw new IllegalArgumentException(name);
				if (definition.getType() == ArgumentDefinition.ArgumentType.FLAG) {
					flags.add(name);
				} else {
					if (i == args.length - 1)
						throw new IllegalArgumentException(name + " expects a value");
					String next = args[i + 1];
					if (next.startsWith(LONG_PREFIX) || next.startsWith(SHORT_PREFIX))
						throw new IllegalArgumentException(name + " expects a value");
					parameters.put(name, next);
					i++;
				}
			} else {
				remaining.add(argument);
			}
		}
		return new Arguments(flags, parameters, remaining);
	}

}
