package org.fnet.osbuilder.arguments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Arguments {

	private final List<String> flags;
	private final Map<String, String> parameters;
	private final List<String> remaining;

	public Arguments() {
		this.flags = new ArrayList<>();
		this.parameters = new HashMap<>();
		this.remaining = new ArrayList<>();
	}

	public Arguments(List<String> flags, Map<String, String> parameters, List<String> remaining) {
		this.flags = flags;
		this.parameters = parameters;
		this.remaining = remaining;
	}

	public List<String> getFlags() {
		return flags;
	}

	public boolean hasFlag(String flag) {
		return flags.contains(flag);
	}


	public Map<String, String> getParameters() {
		return parameters;
	}

	public boolean hasParameter(String param) {
		return parameters.containsKey(param);
	}

	public String getParameter(String param) {
		return parameters.get(param);
	}

	public String getParameter(String param, String defaultValue) {
		return parameters.getOrDefault(param, defaultValue);
	}


	public List<String> getRemaining() {
		return remaining;
	}

}
