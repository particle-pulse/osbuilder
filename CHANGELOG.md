# Changelog

## [1.1] - 2018-10-27

### Added
- Add "offline" mode" (#1)
- Argument parsing
- Pass arguments to target (#2)
- Reimplement NASM (#5)

### Fixes
- Check if the toolchain is really necessary (#3)
- Uninstall previous version of toolchain component if new version requested (#4)
- FIX: Caching works now (#6)

## [1.0] - 2018-10-21
### Added
- Initial release
 
[1.1]: https://gitlab.com/particle-pulse/osbuilder/tags/1.1
[1.0]: https://gitlab.com/particle-pulse/osbuilder/tags/1.0